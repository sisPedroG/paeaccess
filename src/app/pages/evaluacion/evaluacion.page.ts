import { Component, OnInit } from '@angular/core';
import { FormDataService } from 'src/app/provider/form-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-evaluacion',
  templateUrl: './evaluacion.page.html',
  styleUrls: ['./evaluacion.page.scss'],
})
export class EvaluacionPage implements OnInit {

  progressBarValue: number = 0;
  preguntasRespondidas: number = 0;
  cantidadPreguntas: number = 0;
  respuestas: any = [];
  constructor(
    private services: FormDataService,
    private router: Router
  ) { }

  ngOnInit() {

  }
  ionViewWillEnter() {
    this.services.load().subscribe((data: any) => {
      this.respuestas = data.registro.preguntas;
      this.cantidadPreguntas = this.respuestas.length;
      this.progressBar()
      console.log(this.respuestas)
      console.log(data)
    })

  }

  progressBar() {

    this.progressBarValue = this.preguntasRespondidas / this.cantidadPreguntas;
    console.log(`${this.preguntasRespondidas} / ${this.cantidadPreguntas}`)
  }

  preguntaSiguiente() {
    console.log(this.respuestas[0].respuestas)
    
    if (this.respuestas.length > 1) {
      this.preguntasRespondidas++;
      this.respuestas.splice(0, 1);
      this.progressBar();
    } else {
      this.router.navigateByUrl('/inicio');

    }
  }

  limpiarRespuestas(valorParam) {
    console.log("CHECKLIMPIAR"+this.respuestas[0].respuestaMultiple)
    if (this.respuestas[0].respuestaMultiple) {  //Si permite 1 sola respuesta
      this.respuestas[0].respuestas.forEach(respuesta => { 

        respuesta.valor == valorParam ? respuesta.seleccionada = true : respuesta.seleccionada = false;

      });
    }else{
      this.respuestas[0].respuestas.forEach(respuesta => { //Si permite multiples respuestas
        respuesta.valor == valorParam ? respuesta.seleccionada = !respuesta.seleccionada : 0;
      })

    }
  }
}
