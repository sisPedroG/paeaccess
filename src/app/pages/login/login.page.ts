import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { datosLogin } from '../../interfaces/interfaces';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('nroTramite') myInput;
  progressBar: number = 0;
  maxProgress = 7;
  preguntasRespondidas: number = 0;
  dni: string = '';
  datosUsuario:datosLogin={};
  constructor(private barcodeScanner: BarcodeScanner) { }

  ngOnInit() {
  }
  handleLogin(event) {
    console.log(this.progressBar, event);
    this.preguntasRespondidas++;
    this.progressBar = this.preguntasRespondidas / this.maxProgress;
  }
  gotoNextField(event?) {
    console.log(event);
    if (event.key == "Enter") {
      console.log("enter")
      console.log(this.myInput)
      this.myInput.setFocus();
    }
    // if (event.key == "Unidentified") {
    //   let tempDNI=String(this.dni);
    //   this.dni = tempDNI.slice(0,-1)
    //   console.log('period')

    // }
  }
  swallow() {
    console.log("swallowed");
  }

  scanDNI() {
    this.barcodeScanner.scan({ formats: "PDF_417" }).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      let datosTmp:Array<string>;
      let dniViejo:boolean;
      barcodeData.text.charAt(0) =='@'?dniViejo=true:dniViejo=false;
      datosTmp = barcodeData.text.split("@");
      if(dniViejo){
        console.log(datosTmp)
        datosTmp.splice(0,1);
        this.datosUsuario.numeroDNI=parseInt(datosTmp[0]);
        this.datosUsuario.apellido=datosTmp[3];
        this.datosUsuario.nombre=datosTmp[4];
        this.datosUsuario.fechaNacimiento=datosTmp[6];
        // let tmpDate = moment(datosTmp[6],"DD/MM/YYYY");

        // this.datosUsuario.fechaNacimiento= tmpDate.toDate();
      }

      
      console.log(this.datosUsuario)
    }).catch(err => {
      console.log('Error', err);
    });
  }
}
